#pragma once

#ifndef Q_MOC_RUN
#include <ros/ros.h>

#include <rviz/panel.h>
#endif

#include <QPushButton>
#include <QComboBox>
#include <ros/node_handle.h>
#include <ros/publisher.h>
#include <ros/subscriber.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/UInt32.h>

class QLineEdit;
class QSpinBox;

namespace vbr_rviz_plugin
{
  class CommandPanel : public rviz::Panel
  {
    Q_OBJECT
  public:
    explicit CommandPanel(QWidget *parent = nullptr);

    void load(const rviz::Config &config) override;
    void save(rviz::Config config) const override;

  public Q_SLOTS:

  protected Q_SLOTS:

    void lockPath();

    void execute();

    void terminate();

    void readyState();

  protected:
    QPushButton *btn_lock_path_;
    QPushButton *btn_execute_;
    QPushButton *btn_terminate_;
    QPushButton *btn_ready_state_;
    ros::NodeHandle nh_;
    ros::Publisher path_request;
    ros::Publisher command;
    ros::Subscriber sub_;
    geometry_msgs::PoseArray goal_points;
    void setPointMonitor(geometry_msgs::PoseArray poses);
  };

} // end namespace vbr_rviz_plugin
