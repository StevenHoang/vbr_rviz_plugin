#ifndef SET_POINT_TOOL_H
#define SET_POINT_TOOL_H

#include <rviz/tool.h>
#include <ros/node_handle.h>
#include <ros/publisher.h>
#include <geometry_msgs/PoseArray.h>

namespace Ogre
{
  class SceneNode;
  class Vector3;
} // namespace Ogre

namespace rviz
{
  class VectorProperty;
  class VisualizationManager;
  class ViewportMouseEvent;
} // namespace rviz

namespace vbr_rviz_plugin
{

  // Here we declare our new subclass of rviz::Tool.  Every tool
  // which can be added to the tool bar is a subclass of
  // rviz::Tool.
  class SetPointTool : public rviz::Tool
  {
    Q_OBJECT
  public:
    SetPointTool();
    ~SetPointTool();

    virtual void onInitialize();

    virtual void activate();
    virtual void deactivate();

    virtual int processMouseEvent(rviz::ViewportMouseEvent &event);

    virtual void load(const rviz::Config &config);
    virtual void save(rviz::Config config) const;

  private:
    void makeFlag(const Ogre::Vector3 &position);

    std::vector<Ogre::SceneNode *> flag_nodes_;
    Ogre::SceneNode *moving_flag_node_;
    std::string flag_resource_;
    rviz::VectorProperty *current_flag_property_;
    ros::NodeHandle nh_;
    ros::Publisher pub_;
    geometry_msgs::PoseArray goal_points;
    geometry_msgs::Quaternion orientation;
  };

} // end namespace vbr_rviz_plugin

#endif // SET_POINT_TOOL_H
