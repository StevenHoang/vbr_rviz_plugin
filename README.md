# vbr_rviz_plugin

Rviz plugins for Virtual Barrier Experiment Operation
## Prerequisites and Environment Setup
Please have a look at the [virtual_barrier_ros](https://gitlab.com/StevenHoang/virtual_barrier_ros) repo for more details
## Usage
Launch the robot controller via MoveIt
```
roslaunch panda_moveit_config panda_control_moveit_rviz.launch robot_ip:={YOUR_ROBOT_IP}
```
Add the interface components to interact with virtual_barrier_ros:
- CommandPanel : **Panels** -> **Add New Panel** -> **vbr_rviz_plugin** -> *CommandPanel**
- SceneObject : **Display**-> **Add** (this should be located at the bottom of Display panel) -> **Motion Planning** -> **Scene Object** (switch to this tab)
- SetPoint: **"Plus" sign on the top taskbar"** -> **vbr_rviz_plugin** -> **SetPoint**

Video of how to use the interface: Link to be added
